package br.com.produtos.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.produtos.model.EnumProduto;
import br.com.produtos.model.Pedido;


/**
 * @author raphael
 * Destinada a realizar carga de pedido com todos os produtos
 */
public class CargaPedido {
	
	/**
	 * Cria lista de pedido ficticia
	 * @return java.util.List<Pedido>
	 */
	public static List<Pedido>criaPedidos(){
		List<Pedido>lsPedido = new ArrayList<>();
		
		int index =  0;
		for(EnumProduto produto : EnumProduto.values()){
			Pedido pedido = new Pedido();
			
			Calendar calendar = Calendar.getInstance();
			if(index >= 5){
				calendar.add(Calendar.DAY_OF_MONTH, 1);
			}
			pedido.setData(calendar);
			pedido.setProduto(produto.getDescricao());
			pedido.setQuantidadeProduto(1);
			pedido.setUnidade(produto.getUnidade());
			lsPedido.add(pedido);
			index++;
		}
		return lsPedido;
	}
}
