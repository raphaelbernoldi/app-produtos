package br.com.produtos.service;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

import br.com.produtos.model.Consulta;
import br.com.produtos.model.Pedido;

public class OrdenaPedidoByProduto implements OrdenaPedido {

	@Override
	public void ordenaPedidos(Consulta consulta, List<Pedido> lsPedidos) {
		if(consulta.getOrdenacao().toUpperCase().equals(DESC)){
			Collections
			 .sort(lsPedidos, (Pedido p1, Pedido p2) -> p2.getProduto().compareTo(p1.getProduto()));
		}else{
			Collections
			 .sort(lsPedidos, (Pedido p1, Pedido p2) -> p1.getProduto().compareTo(p2.getProduto()));
		}
	}

	@Override
	public void filter(Consulta consulta, List<Pedido> lsPedidos) {
		Predicate<Pedido>predicate = (pedido) -> !pedido.getProduto().toLowerCase().contains(consulta.getProduto().toLowerCase());
		lsPedidos.removeIf(predicate);

	}

	@Override
	public String getCampoFiltrado() {
		return "Produto";
	}

}
