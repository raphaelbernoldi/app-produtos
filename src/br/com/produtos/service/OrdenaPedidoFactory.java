package br.com.produtos.service;

import br.com.produtos.model.Consulta;

/**
 * 
 * @author raphael
 * Cria instancia correta para ordenacao de pedidos
 */
public class OrdenaPedidoFactory {

	public static OrdenaPedido getInstance(Consulta consulta){
		if(consulta.getProduto() == null && consulta.getData() != null ){
			return new OrdenaPedidoByData();
		}
		
		if(consulta.getProduto() != null && consulta.getData() == null){
			return new OrdenaPedidoByProduto();
		}
		return new OrdenaPedidoByProduto();
	}
}
