package br.com.produtos.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import br.com.produtos.model.Consulta;

public class Validador {

	public static Consulta validaConsulta(String[] args) throws IllegalArgumentException{
		if(args.length == 2){
			Consulta consulta = new Consulta();
			consulta.setOrdenacao(args[1]);
			try {
				consulta.setData(converData(args[0]));
			} catch (ParseException e) {
				consulta.setProduto(args[0]);
			}
			return consulta;
		}
		throw new IllegalArgumentException("Numero de parametros invalidos");
	}
	
	public static Calendar converData(String data) throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(simpleDateFormat.parse(data));
		return calendar;
		
	}
}
