package br.com.produtos.service;

import java.util.List;

import br.com.produtos.model.Consulta;
import br.com.produtos.model.Pedido;

public interface OrdenaPedido {

	public static final String DESC = "DESC";
	public static final String ASC = "ASC";
	
	void ordenaPedidos(Consulta consulta, List<Pedido>lsPedidos);
	void filter(Consulta consulta, List<Pedido>lsPedidos);
	String getCampoFiltrado();
}
