package br.com.produtos;

import java.util.List;

import br.com.produtos.model.Consulta;
import br.com.produtos.model.Pedido;
import br.com.produtos.service.CargaPedido;
import br.com.produtos.service.OrdenaPedido;
import br.com.produtos.service.OrdenaPedidoFactory;
import br.com.produtos.service.Validador;

public class Main {

	private static List<Pedido>pedidos;
	
	static{
		pedidos = CargaPedido.criaPedidos();
	}
	
	public static void main(String[] args) {
		try {
			Consulta consultaPedidos = Validador.validaConsulta(args);
			
			OrdenaPedido ordenaPedido = OrdenaPedidoFactory.getInstance(consultaPedidos);
			ordenaPedido.filter(consultaPedidos, pedidos);
			ordenaPedido.ordenaPedidos(consultaPedidos, pedidos);
			
			pedidos.forEach(
					(pedido) -> System.out.println(pedido.toString()) );
			
			if(pedidos.isEmpty()){
				System.out.println("[Nenhum elemento encontrado][Filtrado por "+ordenaPedido.getCampoFiltrado()+"][valor = "+args[0]+"]");
			}
		} catch (IllegalArgumentException e) {
			printErroDefault(e);
		}
	}
	
	private static void printErroDefault(Exception e){
		System.out.println("***************************************************************");
		System.out.println("[Parametros invalidos][" + e.getMessage() + "]");
		System.out.println("[Primeiro parametro][Data no formato dd/MM/yyyy ou Nome do produto]");
		System.out.println("[Segundo parametro][Tipo Ordenacao][ASC ou DESC]");
		System.out.println("[Favor executar novamente]");
		System.out.println("***************************************************************");
	}

}
