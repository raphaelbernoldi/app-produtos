package br.com.produtos.model;


/**
 * @author raphael
 * Simula uma tabela basica de produtos
 */
public enum EnumProduto {

	SAL(1,"Sal", "1kg"),
	ACUCAR(2,"Acucar", "1kg"),
	ARROZ(3,"Arroz", "1kg"),
	FEIJAO(4,"Feijao", "1kg"),
	OLEO(5,"Oleo", "1 Litro"),
	MACARRAO(6,"Macarrao", "1kg"),
	VINAGRE(7,"Vinagre", "1 Litro"),
	CARNE(8,"Carne", "1kg"),
	PEIXE(9,"Peixe", "1kg"),
	REFRIGERANTE(10,"Refrigerante", "2 Litros"),;
	
	private Integer id;
	private String descricao;
	private String unidade;
	
	private EnumProduto(Integer id, String descricao, String unidade) {
		this.id = id;
		this.descricao = descricao;
		this.unidade = unidade;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	
	public EnumProduto getProdutoById(Integer id){
		for(EnumProduto produto : EnumProduto.values()){
			if(produto.getId().equals(id)){
				return produto;
			}
		}
		return null;
	}
	

	
}
