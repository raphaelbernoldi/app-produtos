package br.com.produtos.model;

import java.util.Calendar;


/**
 * @author raphael
 * 
 */
public class Consulta {

	private Calendar data;
	private String produto;
	private String ordenacao;
	
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public String getOrdenacao() {
		return ordenacao;
	}
	public void setOrdenacao(String ordenacao) {
		this.ordenacao = ordenacao;
	}
	
}
