package br.com.produtos.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author raphael
 *
 */
public class Pedido {
	
	private Calendar data;
	private String produto;
	private String unidade;
	private int quantidadeProduto;
	
	public Calendar getData() {
		return data;
	}
	public void setData(Calendar data) {
		this.data = data;
	}
	public String getProduto() {
		return produto;
	}
	public void setProduto(String produto) {
		this.produto = produto;
	}
	public String getUnidade() {
		return unidade;
	}
	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}
	public int getQuantidadeProduto() {
		return quantidadeProduto;
	}
	public void setQuantidadeProduto(int quantidadeProduto) {
		this.quantidadeProduto = quantidadeProduto;
	}
	
	@Override
	public String toString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return "Pedido:{"
					+ "data: "+sdf.format(data.getTime())+","
					+ "produto: "+produto+","
					+ "unidade: "+unidade+","
					+ "quantidadeProduto: "+quantidadeProduto+"}";
	}
	
	
}
